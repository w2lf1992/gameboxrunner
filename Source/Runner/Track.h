// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SegmentBase.h"
#include "GameFramework/Actor.h"
#include "Track.generated.h"

UCLASS()
class RUNNER_API ATrack : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrack();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	float Speed = 200;//Speed of each segment movement.

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	TArray<TSubclassOf<ASegmentBase>> ListOfAllSegments; //A list of all available segments.
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	TArray<TSubclassOf<ASegmentBase>> ListOfRepairSegments; //A list of all segments with repairs.
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	TArray<AActor*> SegmentArray; //Holds all the active segments.


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Segment")
	int SegmentOffsetBounds = 1000;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SegmentTick(float DeltaTime);//My segment tick func.

	UFUNCTION(BlueprintCallable)
	void SpawnSegment();

	UFUNCTION(BlueprintCallable)
	TSubclassOf<ASegmentBase> GetSegmentToSpawn();
};
