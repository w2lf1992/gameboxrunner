// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ScrollBox.h"
#include "MyScrollBox.generated.h"

/**
 * 
 */
UCLASS()
class RUNNER_API UMyScrollBox : public UScrollBox
{
	GENERATED_BODY()

public:

	UMyScrollBox();
	UMyScrollBox* base = this;

	//UMyScrollBox ThisDerivedClass;

	UFUNCTION(BlueprintCallable, Category = "Widget|Panel")
		void MyReplaceChildAt(int32 Index, UWidget* OldContent, UWidget* NewContent);

	//UFUNCTION(BlueprintCallable, Category = "Widget|Panel")
		//virtual bool UPanelWidget::ReplaceChild(UWidget* CurrentChild, UWidget* NewChild) override;
	
};
