// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SegmentBase.generated.h"

UCLASS()
class RUNNER_API ASegmentBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASegmentBase();
	UFUNCTION(BlueprintCallable)
	float GetMaxDamage();

	UFUNCTION(BlueprintCallable)
	float GetMaxRepair();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category= "Stats")
	float MaxRepair;
	UPROPERTY(VisibleAnywhere, Category= "Stats")
	float MaxDamage;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
