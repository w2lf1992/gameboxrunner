// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class RUNNER_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	int32 CarrotMultiplier = 1;
	
public:
	UFUNCTION(BlueprintCallable)
	int32 GetCarrotMultiplier() const
	{
		return CarrotMultiplier;
	}
	
	UFUNCTION(BlueprintCallable)
	void SetCarrotMultiplier(const int32 NewCarrotMultiplier)
	{
		this->CarrotMultiplier = NewCarrotMultiplier;
	}
};
