// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
class AMyPawn;


UCLASS()
class RUNNER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float SideLinesLimit = 250;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float CurrentLineDestination = 0;
	
	UFUNCTION(BlueprintCallable)
	void InterpLeft();

	UFUNCTION(BlueprintCallable)
	void InterpRight();

	//Called upon landing
	UFUNCTION(BlueprintCallable)
	void OnCharacterLanded();
	
	void OnCharacterInitialized();

	//Touch vars
	bool bSwipeIsPressed;
	FVector2D TouchStartPos, TouchEndPos;

protected:

	UPROPERTY(VisibleAnywhere, Category = "References")
	AMyPawn* CharacterRef;

	UPROPERTY()
	FTimerHandle TimerRight;

	UPROPERTY()
	FTimerHandle TimerLeft;

	UPROPERTY()
	FTimerHandle TimerCheckTouch;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category="Movement")
	bool bCustomIsInAir;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Timers")
	FTimerHandle TimerJump;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement")
	float JumpDeltaTime = 0.00466f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement")
	float JumpFlightTime = 0.2f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Movement")
	float JumpDivider = 50.f;
	
	//Used via timer to jump up
	UFUNCTION()
	void LerpBallUp();

	//Used via timer to land
	UFUNCTION()
	void LerpBallDown();
	
	void MoveRight();
	void MoveLeft();
	void Jump();

	//Mobile input funcs
	void Touch();
	void TouchEnd();
	void CheckTouch();

	UFUNCTION(BlueprintCallable)
	void CustomJump();
	
	UFUNCTION(BlueprintCallable)
	void CustomEndJump();
	//void Swipe();
	//void SwipeEnd();
	//void TouchJump(FKey key);
};
