// Fill out your copyright notice in the Description page of Project Settings.
//Deprecated

#pragma once


#include "MyPlayerController.h"
#include "Camera/CameraComponent.h"
DECLARE_LOG_CATEGORY_EXTERN(MainCharacterLog, Error, All);

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
//#include "GameFramework/CharacterMovementComponent.h"
#include "Components/AudioComponent.h"
#include "Track.h"
#include "RunnerCharacter.generated.h"

UCLASS()
class RUNNER_API ARunnerCharacter : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunnerCharacter(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	bool SetIsFrozen(bool NewState);

	UFUNCTION(BlueprintCallable)
	bool GetIsFrozen();
	
	UFUNCTION(BlueprintCallable)
	bool ActivatePowerUp(int NumberOfPowerUp);
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* CameraComponent;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="References")
	UGameInstance* GameInstanceRef;

	//Curent player score
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Stats")
	int32 Score;

	//This value will be added to score every 1 meter
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Stats")
	int32 ScoreGain = 1;

	//Pointer to current track
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="References")
	ATrack* TrackRef;

	//Max durability of a snowball
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Stats")
	float MaxDurability = 60;

	//Current durability of snowball
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Stats")
	float Durability = 29;

	//Handle of timer used in OnOneMeterPassed function
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Timers")
	FTimerHandle TimerScoreUpdate;

	//Handle of timer decreasing durability
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Timers")
	FTimerHandle TimerDurabilityDecrease;

	//Delay used in timer decreasing durability
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Timers")
	float DecreaseDurabilityDelay = 1;

	//Handle of timer, decreasing durability
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Timers")
	FTimerHandle TimerRotation;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Timers")
	float RotationSpeed = 0.001f;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Timers")
	float RotationBallOffset = -0.1f;

	//Handle of timer increasing/decreasing ball scale and Z axis
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Movement")
	FTimerHandle TimerBallMovement;

	UPROPERTY(BlueprintReadWrite, Category="PowerUps")
	bool bIsFrozen = false;

	UPROPERTY(BlueprintReadWrite, Category="PowerUps")
	int32 PowerUpCounter = 10;

	UPROPERTY(BlueprintReadWrite, Category="PowerUps")
	FTimerHandle TimerPowerUps;
	
	UPROPERTY()
	AMyPlayerController* PlayerControllerRef;

	//Increment player score by Value
	UFUNCTION(BlueprintCallable)
	void IncrementScore(int Value);

	//Increases player score, called every meter
	UFUNCTION(BlueprintCallable)
	void OnOneMeterTraveled();

	//Exposes OnOneMeterTraveled to BP. Is being called every 1 meter.
	UFUNCTION(BlueprintNativeEvent)
	void OnOneMeterTraveled_BP();

	//Decreases snowball durability, called every DecreaseDurabilityDelay
	UFUNCTION(BlueprintCallable)
	void DecreaseDurability();

	//Called upon start of the run, initializes all timers
	UFUNCTION(BlueprintCallable)
	void StartGame();

	//Called upon game over
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Death();
	virtual void Death_Implementation();

	//Called every 0.01 second to add rotation
	UFUNCTION(BlueprintCallable)
	void RotateBall();
	
	//Called by timer to lerp ball size and Z axis position
	UFUNCTION(BlueprintCallable, Category = "Movement")
	void InterpBallUp();

	//Called by timer to lerp ball size and Z axis position
	UFUNCTION(BlueprintCallable, Category = "Movement")
	void InterpBallDown();

	UFUNCTION(BlueprintCallable, Category = "PowerUps")
	void CountdownPowerUp();


	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
//	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Mesh of the ball
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* BallMesh;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category="Stats")
	bool bIsBig = false;

	//Handle of timer which lerps ball left
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Movement")
	FTimerHandle TimerInterpLeft;

	//Handle of timer which lerps ball right
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Movement")
	FTimerHandle TimerInterpRight;

	//Roll Sound 
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Sound")
	UAudioComponent* MyRollSound;

	//Pass negative numbers to decrease durability
	UFUNCTION(BlueprintCallable)
	float AddDurability(float Value);

	UFUNCTION(BlueprintCallable)
	float GetDurability();
	
	UFUNCTION(BlueprintCallable)
	float GetMaxDurability();

	//Called upon changing ball state
	UFUNCTION(BlueprintCallable)
	void SetBallState(bool bIncrease);

};
