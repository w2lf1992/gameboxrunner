// Fill out your copyright notice in the Description page of Project Settings.


#include "Track.h"
#include "Public/MyPawn.h"
#include "SegmentBase.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h" //Math lib

// Sets default values
ATrack::ATrack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATrack::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATrack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SegmentTick(DeltaTime);//My segment tick func.

}

void ATrack::SegmentTick(float DeltaTime)
{
	float DeltaSpeed = UKismetMathLibrary::Multiply_FloatFloat(Speed, DeltaTime);//move independently from fps.
	float X = UKismetMathLibrary::Multiply_FloatFloat(DeltaSpeed,-1);//Invert
	for (AActor* MyActor:SegmentArray)
	{
		MyActor->AddActorWorldOffset(FVector(X,0,0),true); //Panner each segment and sweep
	}
}

void ATrack::SpawnSegment()
{
	if (ListOfAllSegments.Num() != 0)//Is valid. To not crash if list is empty.
	{
		TSubclassOf<ASegmentBase> MyClass;
		FVector SpawnLocation = FVector(0);
		FRotator SpawnRotation = FRotator(0);

		FActorSpawnParameters SpawnParams;
		//SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		if (SegmentArray.Num() == 0)//if array empty
		{
			//Initial segment spawn
			MyClass = ListOfAllSegments[0]; //Select segment without obsticles. First element.
		}
		else
		{
			//Else spawn random segment from ListOfAllSegments and offset location. Except the first element.
			MyClass = GetSegmentToSpawn();
			SpawnLocation = FVector(UKismetMathLibrary::Add_FloatFloat(SegmentArray[SegmentArray.Num() - 1]->GetActorLocation().X, SegmentOffsetBounds), 0, 0); //Offset Segment
		}
		
		AActor* SpawnedActor = GetWorld()->SpawnActor(MyClass, &SpawnLocation, &SpawnRotation, SpawnParams);//Spawn segment actor
		SegmentArray.Add(SpawnedActor);//Add it to array to keep track of it.
	}
}

TSubclassOf<ASegmentBase> ATrack::GetSegmentToSpawn()
{
	TSubclassOf<ASegmentBase> Segment;
	if (Cast<AMyPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0))->GetDurability() < 20)
	{
		Segment = ListOfRepairSegments[UKismetMathLibrary::RandomIntegerInRange(1, (ListOfRepairSegments.Num() - 1))];
	}
	else
	{
		Segment = ListOfAllSegments[UKismetMathLibrary::RandomIntegerInRange(0, (ListOfAllSegments.Num() - 1))];
	}
	return Segment;
}
