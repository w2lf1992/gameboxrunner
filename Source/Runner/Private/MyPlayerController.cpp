// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "MyPawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"


//Called when character is loaded to get pointer to object and bind inputs
void AMyPlayerController::OnCharacterInitialized()
{
	//Left this for keyboard testing.
	InputComponent->BindAction("MoveRight", IE_Pressed, this, &AMyPlayerController::MoveRight);
	InputComponent->BindAction("MoveLeft", IE_Pressed, this, &AMyPlayerController::MoveLeft);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AMyPlayerController::Jump);

	//Mobile input
	InputComponent->BindAction("Touch", IE_Pressed, this, &AMyPlayerController::Touch);

	CharacterRef = Cast<AMyPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

//Call to move ball left
void AMyPlayerController::InterpLeft()
{
	if (CharacterRef->GetActorLocation().Y > CurrentLineDestination)
	{
		CharacterRef->AddActorWorldOffset({0, -10, 0});
		CharacterRef->BallMesh->AddWorldRotation({0, 0, -1.5f});
	}
	else
	{
		if (CharacterRef->TimerInterpLeft.IsValid())
		{
			GetWorldTimerManager().ClearTimer(CharacterRef->TimerInterpLeft);
			CharacterRef->TimerInterpLeft.Invalidate();
		}
		else
		{
			GetWorldTimerManager().ClearTimer(TimerLeft);
			TimerLeft.Invalidate();
		}
	}
}

//Call to move ball right
void AMyPlayerController::InterpRight()
{
	if (CharacterRef->GetActorLocation().Y < CurrentLineDestination)
	{
		CharacterRef->AddActorWorldOffset({0, 10, 0});
		CharacterRef->BallMesh->AddWorldRotation({0, 0, 1.5f});
	}
	else
	{
		if (CharacterRef->TimerInterpRight.IsValid())
		{
			GetWorldTimerManager().ClearTimer(CharacterRef->TimerInterpRight);
			CharacterRef->TimerInterpRight.Invalidate();
		}
		else
		{
			GetWorldTimerManager().ClearTimer(TimerRight);
			TimerRight.Invalidate();
		}
	}
}

void AMyPlayerController::OnCharacterLanded()
{
}

//Called by player input, checks if ball can move right and calls InterpRight if true
void AMyPlayerController::MoveRight()
{
	if ((CharacterRef->bIsBig) && (CurrentLineDestination < SideLinesLimit / 2) || !(CharacterRef->bIsBig) && (
		CurrentLineDestination < SideLinesLimit))
	{
		CurrentLineDestination += SideLinesLimit;
		GetWorldTimerManager().SetTimer(TimerRight, this, &AMyPlayerController::InterpRight, 0.01, true, 0);
		ClientPlaySound(CharacterRef->JumpSound); //Play sound.
	}
}

void AMyPlayerController::MoveLeft()
{
	if ((CharacterRef->bIsBig) && (CurrentLineDestination > -SideLinesLimit / 2) || (!(CharacterRef->bIsBig) && (
		CurrentLineDestination > -SideLinesLimit)))
	{
		CurrentLineDestination -= SideLinesLimit;
		GetWorldTimerManager().SetTimer(TimerLeft, this, &AMyPlayerController::InterpLeft, 0.01, true, 0);
		ClientPlaySound(CharacterRef->JumpSound); //Play sound.
	}
}

//Deprecated
void AMyPlayerController::Jump()
{
	//CharacterRef->Jump(); //Built in jump. Change value in character movement component.
	CustomJump();
}


//Mobile inputs
void AMyPlayerController::Touch()
{
	GetInputTouchState(ETouchIndex::Touch1, TouchStartPos.X, TouchStartPos.Y, bSwipeIsPressed);
	//This gets and sets our vars at the same time.
	//call CheckTouch in 0.1 sec if its swipe or tap.
	GetWorldTimerManager().SetTimer(TimerCheckTouch, this, &AMyPlayerController::CheckTouch, 0.1f, false, -1);
}

void AMyPlayerController::TouchEnd()
{
}

void AMyPlayerController::CheckTouch()
{
	GetInputTouchState(ETouchIndex::Touch1, TouchEndPos.X, TouchEndPos.Y, bSwipeIsPressed);
	//This gets and sets our vars at the same time.
	FVector2D SwipeVector = TouchStartPos - TouchEndPos;

	if (bSwipeIsPressed && SwipeVector.Size() > 15) //swipe
	{
		if (abs(SwipeVector.X) > abs(SwipeVector.Y))
		{
			if (SwipeVector.X > 0) //SwipeLeft
			{
				MoveLeft();
			}
			else if (SwipeVector.X < 0) //SwipeRight
			{
				MoveRight();
			}
		}
		else if (SwipeVector.Y > 0) //SwipeUp
		{
			CustomJump();
		}
		else if (SwipeVector.Y < 0) //SwipeDown
		{
			CustomEndJump();
		}
	}
	else if (!bSwipeIsPressed) //tap
	{
		if ((CharacterRef->GetDurability() > CharacterRef->GetMaxDurability() / 2) && !(CharacterRef->bIsBig))
		{
			
		}
	}
	else //Try again if nothing has been done.
	{
		GetWorldTimerManager().SetTimer(TimerCheckTouch, this, &AMyPlayerController::CheckTouch, 0.1f, false, -1);
	}
}

void AMyPlayerController::CustomJump()
{
	if (!bCustomIsInAir)
	{
		GetWorldTimerManager().SetTimer(TimerJump, this, &AMyPlayerController::LerpBallUp, JumpDeltaTime, true, 0);
		bCustomIsInAir = true;
		CharacterRef->MyRollSound->SetPaused(true); //Pause roll sound on jump.
		ClientPlaySound(CharacterRef->JumpSound); //Play jump sound.
	}
}

void AMyPlayerController::CustomEndJump()
{
	if (bCustomIsInAir)
	{
		GetWorldTimerManager().ClearTimer(TimerJump);
		TimerJump.Invalidate();
		GetWorldTimerManager().SetTimer(TimerJump, this, &AMyPlayerController::LerpBallDown, JumpDeltaTime, true, 0);
		CharacterRef->MyRollSound->SetPaused(false); //Unpause roll sound on endjump.
		ClientPlaySound(CharacterRef->LandSound); //Play land sound.
	}
}

void AMyPlayerController::LerpBallUp()
{
	float Offset;

	if (CharacterRef->bIsBig)
	{
		if (CharacterRef->GetActorLocation().Z >= 326)
		{
			GetWorldTimerManager().ClearTimer(TimerJump);
			TimerJump.Invalidate();
			FTimerHandle Handle;
			GetWorldTimerManager().SetTimer(Handle, this, &AMyPlayerController::CustomEndJump, JumpFlightTime, false,
			                                -1);
			return;
		}
		Offset = (326 - CharacterRef->GetActorLocation().Z) / JumpDivider;
	}
	else
	{
		
		if (CharacterRef->GetActorLocation().Z >= 228)
		{
			GetWorldTimerManager().ClearTimer(TimerJump);
			TimerJump.Invalidate();
			FTimerHandle Handle;
			GetWorldTimerManager().SetTimer(Handle, this, &AMyPlayerController::CustomEndJump, JumpFlightTime, false,
			                                -1);
			return;
		}
		Offset = (228 - CharacterRef->GetActorLocation().Z) / JumpDivider;
	}
	Offset = UKismetMathLibrary::FClamp(Offset, 1.0f, 2.5f);
	CharacterRef->AddActorLocalOffset({0, 0, Offset});
}

void AMyPlayerController::LerpBallDown()
{
	float Offset;

	if (CharacterRef->bIsBig)
	{
		if (CharacterRef->GetActorLocation().Z <= 176)
		{
			GetWorldTimerManager().ClearTimer(TimerJump);
			TimerJump.Invalidate();
			bCustomIsInAir = false;
			OnCharacterLanded();
			return;
		}
		Offset = (326 - CharacterRef->GetActorLocation().Z) / JumpDivider;
	}
	else
	{
		if (CharacterRef->GetActorLocation().Z <= 78)
		{
			GetWorldTimerManager().ClearTimer(TimerJump);
			TimerJump.Invalidate();
			bCustomIsInAir = false;
			OnCharacterLanded();
			return;
		}
		Offset = (228 - CharacterRef->GetActorLocation().Z) / JumpDivider;
	}
	Offset = UKismetMathLibrary::FClamp(Offset, 1, 2.5);
	CharacterRef->AddActorLocalOffset({0, 0, -Offset});
}
