// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleBase.h"
#include "MyPawn.h"
#include "Kismet/GameplayStatics.h"
#include "Runner/RunnerCharacter.h"


// Sets default values
AObstacleBase::AObstacleBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Hitbox = CreateDefaultSubobject<UBoxComponent>("ShapeComponent");
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");

	Hitbox->OnComponentBeginOverlap.AddDynamic(this, &AObstacleBase::OnObstacleHit);
	if(this->SetRootComponent(Mesh))
	{
		Hitbox->SetupAttachment(this->GetRootComponent());

	}
}

void AObstacleBase::OnObstacleHit(UPrimitiveComponent* OverlappedComponent, 
								AActor* OtherActor, 
								UPrimitiveComponent* OtherComp, 
								int32 OtherBodyIndex, 
								bool bFromSweep, 
								const FHitResult &SweepResult )
{
	if(Cast<AMyPawn>(OtherActor))
	{
		Cast<AMyPawn>(OtherActor)->AddDurability(-Damage);
		//Call BP func
		OnObstacleHit_BP(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
		Destroy();
	}
}

void AObstacleBase::OnObstacleHit_BP_Implementation(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	// in BP
}

// Called when the game starts or when spawned
void AObstacleBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AObstacleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
