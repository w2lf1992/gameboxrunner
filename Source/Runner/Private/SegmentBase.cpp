// Fill out your copyright notice in the Description page of Project Settings.


#include "SegmentBase.h"

// Sets default values
ASegmentBase::ASegmentBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

float ASegmentBase::GetMaxDamage()
{
	return MaxDamage;
}

float ASegmentBase::GetMaxRepair()
{
	return MaxRepair;
}

// Called when the game starts or when spawned
void ASegmentBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASegmentBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

