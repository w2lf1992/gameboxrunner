// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"

#include "CollisionAnalyzer/Public/ICollisionAnalyzer.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(MainPawnLog);

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SphereCollider = CreateDefaultSubobject<USphereComponent>("Collider");
	this->SetRootComponent(SphereCollider);
	BallMesh->SetupAttachment(GetRootComponent());
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(GetRootComponent());
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	PlayerControllerRef = Cast<AMyPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	PlayerControllerRef->OnCharacterInitialized();
}
bool AMyPawn::SetIsFrozen(bool NewState)
{
	this->bIsFrozen = NewState;
	return bIsFrozen;
}

bool AMyPawn::GetIsFrozen()
{
	return bIsFrozen;
}

bool AMyPawn::ActivatePowerUp(int NumberOfPowerUp)
{
	if(NumberOfPowerUp == -1)
	{
		return false;
	}
	switch (NumberOfPowerUp)
	{
	case 0:
		{
			Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->SetCarrotMultiplier(2);
			GetWorldTimerManager().SetTimer(TimerPowerUps, this, &AMyPawn::CountdownPowerUp, 1.f, true, 0);
			break;
		}
	case 1:
		{
			bIsFrozen = true;
			GetWorldTimerManager().SetTimer(TimerPowerUps, this, &AMyPawn::CountdownPowerUp, 1.f, true, 0);
			break;
		}
	case 2:
		{
			Durability = MaxDurability;
			break;
		}
		default:
			return false;
	}
	return true;
}

void AMyPawn::IncrementScore(int Value)
{
	this->Score += Value;
}

void AMyPawn::OnOneMeterTraveled()
{
	IncrementScore(ScoreGain);
	if (TrackRef)
	{
		//TODO maybe to also divide Time by 100 to get true 1 unit. (In project settings I have 1meter = 1unit.)
		float const Time = 10 / TrackRef->Speed;
		OnOneMeterTraveled_BP();
		GetWorldTimerManager().SetTimer(TimerScoreUpdate, this, &AMyPawn::OnOneMeterTraveled, Time, false, 0);	
	}
	else
	{
		UE_LOG(MainPawnLog, Warning, TEXT("TrackRef is invalid"));
	}
}

void AMyPawn::OnOneMeterTraveled_BP_Implementation()
{
	//In BP
}

void AMyPawn::DecreaseDurability()
{
	if (!bIsFrozen)
	{
		this->Durability -= 1;
	}
	if (Durability <= 0) 
	{
		Death();
	}
}

void AMyPawn::StartGame()
{
	GetWorldTimerManager().SetTimer(TimerDurabilityDecrease, this, &AMyPawn::DecreaseDurability,
	                                DecreaseDurabilityDelay, true, 0);
	OnOneMeterTraveled();
	GetWorldTimerManager().SetTimer(TimerRotation, this, &AMyPawn::RotateBall, RotationSpeed, true, 0);
}


void AMyPawn::Death_Implementation()
{
	//In BP
}

float AMyPawn::AddDurability(float Value)
{
	Durability += Value;

	if(Durability>=MaxDurability)
	{
		this->Durability = MaxDurability;
		if((!bIsBig)&&!(TimerBallMovement.IsValid())) SetBallState(true);
		FTimerHandle MyHandle;
		GetWorldTimerManager().SetTimer(MyHandle, this, &AMyPawn::SetBallStateSmall, 10, false, -1);
	}
	return Durability;
}

float AMyPawn::GetDurability()
{
	return this->Durability;
}

float AMyPawn::GetMaxDurability()
{
	return MaxDurability;
}
void AMyPawn::RotateBall()
{
	BallMesh->AddWorldRotation({RotationBallOffset, 0, 0});
	//GetWorldTimerManager().SetTimer(TimerRotation, this, &::RotateBall, RotationSpeed, false, 0);
}

void AMyPawn::SetBallState(bool bIncrease)
{
	if (bIncrease)
	{
		this->bIsBig = true;
		GetWorldTimerManager().SetTimer(TimerBallMovement, this, &AMyPawn::InterpBallUp, 0.007f, true, 0);
		if (UKismetMathLibrary::NearlyEqual_FloatFloat(this->GetActorLocation().Y, 0))
		{
			PlayerControllerRef->CurrentLineDestination = 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);

		}
		else if (this->GetActorLocation().Y < 0)
		{
			PlayerControllerRef->CurrentLineDestination += 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);
		}
		else
		{
			PlayerControllerRef->CurrentLineDestination -= 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpLeft, PlayerControllerRef, &AMyPlayerController::InterpLeft, 0.01, true, 0);
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("Increased move requested"));
		}
	}
	else
	{
		bIsBig = false;
		GetWorldTimerManager().SetTimer(TimerBallMovement, this, &AMyPawn::InterpBallDown, 0.007f, true, 0);
		if (this->GetActorLocation().Y > 0)
		{
			PlayerControllerRef->CurrentLineDestination = 0.f;
			GetWorldTimerManager().SetTimer(TimerInterpLeft, PlayerControllerRef, &AMyPlayerController::InterpLeft, 0.01, true, 0);
		}
		else if (this->GetActorLocation().Y < 0)
		{
			PlayerControllerRef->CurrentLineDestination = 0.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);
		}
	}
}

void AMyPawn::InterpBallUp()
{
		if(BallMesh->GetRelativeScale3D().X < 3.3)
		{
			this->BallMesh->SetRelativeScale3D(BallMesh->GetRelativeScale3D().operator+({0.01, 0.01, 0.01}));
			//Cast<UCapsuleComponent>(this->GetRootComponent())->SetCapsuleHalfHeight(Cast<UCapsuleComponent>(this->GetRootComponent())->GetUnscaledCapsuleHalfHeight()+0.542f);
			CameraComponent->SetRelativeLocation(CameraComponent->GetRelativeLocation().operator+({-1.722f, 0, 0.833f}));
		}
		else
		{
			GetWorldTimerManager().ClearTimer(TimerBallMovement);
			TimerBallMovement.Invalidate();
		}
}

void AMyPawn::InterpBallDown()
{
	if(BallMesh->GetRelativeScale3D().X > 1.5)
	{
		this->BallMesh->SetRelativeScale3D(BallMesh->GetRelativeScale3D().operator-({0.01, 0.01, 0.01}) );
		//Cast<UCapsuleComponent>(this->GetRootComponent())->SetCapsuleHalfHeight(Cast<UCapsuleComponent>(this->GetRootComponent())->GetUnscaledCapsuleHalfHeight()-0.542f);
		CameraComponent->SetRelativeLocation(CameraComponent->GetRelativeLocation().operator-({-1.722f, 0, 0.833f}));
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerBallMovement);
		TimerBallMovement.Invalidate();
	}
}

void AMyPawn::CountdownPowerUp()
{
	PowerUpCounter--;
	if (PowerUpCounter <= 0)
	{
		GetWorldTimerManager().ClearTimer(TimerPowerUps);
		TimerPowerUps.Invalidate();
		PowerUpCounter = 10;
		Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->SetCarrotMultiplier(1);
		bIsFrozen = false;
	}
}

void AMyPawn::SetBallStateSmall()
{
	SetBallState(false);
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

