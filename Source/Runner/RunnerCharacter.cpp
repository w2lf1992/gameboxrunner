// Fill out your copyright notice in the Description page of Project Settings.
//Deprecated

#include "RunnerCharacter.h"

#include "MyGameInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "RunnerCharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

class  AMyPlayerController;

DEFINE_LOG_CATEGORY(MainCharacterLog);
//Use my RunnerCharacterMovementComponent instead of default
// Sets default values
ARunnerCharacter::ARunnerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<URunnerCharacterMovementComponent>(
		ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	this->SetRootComponent(BallMesh);
	//BallMesh->SetupAttachment(GetMesh());
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(GetRootComponent());
}

bool ARunnerCharacter::SetIsFrozen(bool NewState)
{
	this->bIsFrozen = NewState;
	return bIsFrozen;
}

bool ARunnerCharacter::GetIsFrozen()
{
	return bIsFrozen;
}

bool ARunnerCharacter::ActivatePowerUp(int NumberOfPowerUp)
{
	if(NumberOfPowerUp == -1)
	{
		return false;
	}
	switch (NumberOfPowerUp)
	{
	case 0:
		{
			Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->SetCarrotMultiplier(2);
			GetWorldTimerManager().SetTimer(TimerPowerUps, this, &ARunnerCharacter::CountdownPowerUp, 1.f, true, 0);
			break;
		}
	case 1:
		{
			bIsFrozen = true;
			GetWorldTimerManager().SetTimer(TimerPowerUps, this, &ARunnerCharacter::CountdownPowerUp, 1.f, true, 0);
			break;
		}
	case 2:
		{
			Durability = MaxDurability;
			break;
		}
		default:
			return false;
	}
	return true;
}

// Called when the game starts or when spawned
void ARunnerCharacter::BeginPlay()
{
	Super::BeginPlay();
	PlayerControllerRef = Cast<AMyPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	PlayerControllerRef->OnCharacterInitialized();
}

void ARunnerCharacter::IncrementScore(int Value)
{
	this->Score += Value;
}

void ARunnerCharacter::OnOneMeterTraveled()
{
	IncrementScore(ScoreGain);
	if (TrackRef)
	{
		//TODO Need to get the actual 1 meter traveled. (Atm this is to fast. Each segment is about 10meters. Right now we are doing 100m in 1 segment.)
		float Time = 10 / TrackRef->Speed;
		OnOneMeterTraveled_BP();
		GetWorldTimerManager().SetTimer(TimerScoreUpdate, this, &ARunnerCharacter::OnOneMeterTraveled, Time, false, 0);	
	}
	else
	{
		UE_LOG(MainCharacterLog, Warning, TEXT("TrackRef is invalid"));
	}
}

void ARunnerCharacter::OnOneMeterTraveled_BP_Implementation()
{
	//In BP
}

void ARunnerCharacter::DecreaseDurability()
{
	if (!bIsFrozen)
	{
		this->Durability -= 1;
	}
	if (Durability <= 0) 
	{
		Death();
	}
}

void ARunnerCharacter::StartGame()
{
	GetWorldTimerManager().SetTimer(TimerDurabilityDecrease, this, &ARunnerCharacter::DecreaseDurability,
	                                DecreaseDurabilityDelay, true, 0);
	OnOneMeterTraveled();
	GetWorldTimerManager().SetTimer(TimerRotation, this, &ARunnerCharacter::RotateBall, RotationSpeed, true, 0);
}


void ARunnerCharacter::Death_Implementation()
{
	//In BP
}

float ARunnerCharacter::AddDurability(float Value)
{
	Durability += Value;
	if(Durability < MaxDurability/2)
	{
		if(bIsBig)
		{
			SetBallState(false);
		}
	}
	if(Durability>MaxDurability)
	{
		this->Durability = MaxDurability;
	}
	return Durability;
}

float ARunnerCharacter::GetDurability()
{
	return this->Durability;
}

float ARunnerCharacter::GetMaxDurability()
{
	return MaxDurability;
}
void ARunnerCharacter::RotateBall()
{
	BallMesh->AddWorldRotation({RotationBallOffset, 0, 0});
	//GetWorldTimerManager().SetTimer(TimerRotation, this, &ARunnerCharacter::RotateBall, RotationSpeed, false, 0);
}

void ARunnerCharacter::SetBallState(bool bIncrease)
{
	if (bIncrease)
	{
		GetWorldTimerManager().SetTimer(TimerBallMovement, this, &ARunnerCharacter::InterpBallUp, 0.007f, true, 0);
		//BallMesh->SetRelativeScale3D({3.3, 3.3, 3.3});
		this->bIsBig = true;
		if (UKismetMathLibrary::NearlyEqual_FloatFloat(this->GetActorLocation().Y, 0))
		{
			PlayerControllerRef->CurrentLineDestination = 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);

		}
		else if (this->GetActorLocation().Y < 0)
		{
			PlayerControllerRef->CurrentLineDestination += 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);
		}
		else
		{
			PlayerControllerRef->CurrentLineDestination -= 125.f;
			GetWorldTimerManager().SetTimer(TimerInterpLeft, PlayerControllerRef, &AMyPlayerController::InterpLeft, 0.01, true, 0);
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("Increased move requested"));
		}
	}
	else
	{
		GetWorldTimerManager().SetTimer(TimerBallMovement, this, &ARunnerCharacter::InterpBallDown, 0.007f, true, 0);
		//BallMesh->SetRelativeScale3D({1.5, 1.5, 1.5});
		bIsBig = false;
		if (this->GetActorLocation().Y > 0)
		{
			PlayerControllerRef->CurrentLineDestination = 0.f;
			GetWorldTimerManager().SetTimer(TimerInterpLeft, PlayerControllerRef, &AMyPlayerController::InterpLeft, 0.01, true, 0);
		}
		else if (this->GetActorLocation().Y < 0)
		{
			PlayerControllerRef->CurrentLineDestination = 0.f;
			GetWorldTimerManager().SetTimer(TimerInterpRight, PlayerControllerRef, &AMyPlayerController::InterpRight, 0.01, true, 0);
		}
	}
}

void ARunnerCharacter::InterpBallUp()
{
		if(BallMesh->GetRelativeScale3D().X < 3.3)
		{
			this->BallMesh->SetRelativeScale3D(BallMesh->GetRelativeScale3D().operator+({0.01, 0.01, 0.01}) );
			Cast<UCapsuleComponent>(this->GetRootComponent())->SetCapsuleHalfHeight(Cast<UCapsuleComponent>(this->GetRootComponent())->GetUnscaledCapsuleHalfHeight()+0.542f);
			CameraComponent->SetRelativeLocation(CameraComponent->GetRelativeLocation().operator+({-1.722f, 0, 0.833f}));
		}
		else
		{
			GetWorldTimerManager().ClearTimer(TimerBallMovement);
			TimerBallMovement.Invalidate();
		}
}

void ARunnerCharacter::InterpBallDown()
{
	if(BallMesh->GetRelativeScale3D().X > 1.5)
	{
		this->BallMesh->SetRelativeScale3D(BallMesh->GetRelativeScale3D().operator-({0.01, 0.01, 0.01}) );
		Cast<UCapsuleComponent>(this->GetRootComponent())->SetCapsuleHalfHeight(Cast<UCapsuleComponent>(this->GetRootComponent())->GetUnscaledCapsuleHalfHeight()-0.542f);
		CameraComponent->SetRelativeLocation(CameraComponent->GetRelativeLocation().operator-({-1.722f, 0, 0.833f}));
	}
	else
	{
		GetWorldTimerManager().ClearTimer(TimerBallMovement);
		TimerBallMovement.Invalidate();
	}
}

void ARunnerCharacter::CountdownPowerUp()
{
	PowerUpCounter--;
	if (PowerUpCounter <= 0)
	{
		GetWorldTimerManager().ClearTimer(TimerPowerUps);
		TimerPowerUps.Invalidate();
		PowerUpCounter = 10;
		Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->SetCarrotMultiplier(1);
		bIsFrozen = false;
	}
}


// Called every frame
void ARunnerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
//	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
//	CharacterComp->UpdateBasedMovement(DeltaTime);
}

// Called to bind functionality to input
//void ARunnerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
//}